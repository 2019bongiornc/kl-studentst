import numpy as np
import scipy.stats as st
from scipy.optimize import minimize

class KLMonte:
	def __init__(self,C,nu,Nboot=10000,eigvect=None):
		'''
		Initialize the KL(C||H): information lost if H is used to approx C.
		
		Variables:
			C: Population matrix covariance
			nu: Degree-of-freedom of the t-student distribution
			Nboot: Number of samples in the Monte Carlo apprixmation [default 10000]
		
		Optional:
			eigvect: eigenvectors of the RIE;
		'''

		self.C = C
		self.n = C.shape[0]
		self.nu = nu
		self.eigvect=eigvect

		Q = st.multivariate_t.rvs(shape=C,df=nu,size=Nboot)

		self.Qt = Q.reshape((Nboot,self.n,1))
		self.Q = Q.reshape((Nboot,1,self.n))

		Cinv = np.linalg.inv(C)
		lc = np.linalg.eigvalsh(C)

		self.Pterm = -0.5*(self.n+nu)*np.log(1+1/nu*(self.Q @ Cinv @ self.Qt)).mean()
		self.logp = -0.5*np.log(lc).sum()
		return

	def compute(self,H,norm=True):
		'''
		it computes the KL(C||H) with Monte Carlo: information lost if H is used to approximate C.
		
		H: Estimator of the covariance matrix
		norm: Normalize the KL by the system size [default True]
		'''

		Hinv = np.linalg.inv(H)
		lh = np.linalg.eigvalsh(H)

		logq = 0.5*np.log(lh).sum()
		Qterm = 0.5*(self.n+self.nu)*np.log(1+1/self.nu*(self.Q @ Hinv @ self.Qt)).mean()
		kl = Qterm+logq+self.Pterm+self.logp
		if norm==True:
			return kl/self.n
		else:
			return kl


	def compute_formeigh(self,lh,norm=True):
		'''
		it computes the KL(C||Xi(lh)) with Monte Carlo from the initialized eigenvectors
			
		lh: eigenvalues of the RIE
		norm: Normalize the KL by the system size [default True]
		'''

		Hinv = self.eigvect @ np.diag(1/lh) @ self.eigvect.T

		logq = 0.5*np.log(lh).sum()
		Qterm = 0.5*(self.n+self.nu)*np.log(1+1/self.nu*(self.Q @ Hinv @ self.Qt)).mean()
		kl = Qterm+logq+self.Pterm+self.logp
		if norm==True:
			return kl/self.n
		else:
			return kl




	def OptimalEig(self,l0):   
		'''
		Find numerically the eigenvalues that minimizes KL(C||Xi(lh)) with Monte Carlo and SLQP.
		It can be slow, and can be trapped in local minima. You must provide the eigenvectors.
			
		l0: initial guess of eigenvalues of the RIE. Use the oracle or np.ones(n).
		'''

		out = minimize(self.compute_formeigh,l0,bounds=[(1e-10,self.n-1e-10)]*self.n,constraints=({'type':'eq','fun':lambda x: x.sum()-self.n},))
		return out
			
    
def KLapprox(C,H,h=None,norm=True):
	'''
	Asymptotic approximation of KL(C||H): information lost if H is used to approximate C.
	
	Optinal Variables:
		h: ration nu/n, where nu is the degree-of-freedom of the t-student
		norm:ormalize the KL by the system size [default True]
	'''

	n = H.shape[0]

	ls = np.linalg.eigvalsh(H)
	lpop = np.linalg.eigvalsh(C)
	Csinv = np.linalg.inv(H)

	if h==None:
		klnorm = 0.5*(np.log(ls).sum()/n - np.log(lpop).sum()/n + np.log( (Csinv*C).sum()/n ))
		if norm==True:
			return klnorm
		else:
			return klnorm*n
	else:
		klnorm = 0.5*(np.log(ls).sum()/n - np.log(lpop).sum()/n + (1+h)*np.log(h+ (Csinv*C).sum()/n )-(1+h)*np.log(1+h))
		if norm==True:
			return klnorm
		else:
			return klnorm*n

def KLnorm(C,H,norm=True):
	'''
	KL(C||H) for multivariate normal variables: information lost if H is used to approximate C.
	
	Optinal Variables:
		norm:ormalize the KL by the system size [default True]
	'''
	lh,vh = np.linalg.eigh(H)
	Hinv = vh @ np.diag(1/lh) @ vh.T

	lc = np.linalg.eigvalsh(C)

	n = C.shape[0]

	kl = 0.5*((Hinv * C).sum()-n-np.log(lc).sum()+np.log(lh).sum())
	if norm==True:
		return kl/n
	else:
		return kl 

    
