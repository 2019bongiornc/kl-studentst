
# KL-Divergence Monte Carlo Estimation

## Getting started
This repository provides a Python script for the estimation of the Kullback-Leibler (KL) divergence between a population covariance matrix and its estimator via Monte Carlo methods.

The main class KLMonte is initialized with a population covariance matrix, degrees of freedom of a t-distribution, and an optional eigenvector argument. The KL divergence for a given estimator can be computed using the compute method. If the eigenvectors were provided during the initialization, the KL divergence can be calculated using the compute_formeigh method. The OptimalEig method finds the eigenvalues that minimize the KL divergence.

Three additional functions are provided: KLapprox, KLnorm, and KLmultivariate. The KLapprox function calculates the asymptotic approximation of the KL divergence. The KLnorm function calculates the KL divergence for multivariate normal variables.

## Requirements
numpy;
scipy

## Usage
The following script is an example of usage:
```
import numpy as np
import scipy.stats as st
from KL import KLMonte, KLapprox, KLnorm

# Generate a 5x5 population covariance matrix
n,nu,t = 5,10,20
C = np.corrcoef(np.random.rand(n,t))

# Initialize the KL divergence calculator
kl_monte = KLMonte(C, nu=10, Nboot=10000)

# Generate a 5x5 estimator covariance matrix
X = st.multivariate_t.rvs(shape=C,df=nu,size=t).T
H = np.corrcoef(C)

# Compute the KL divergence
kl_divergence = kl_monte.compute(H)
```
Please refer to the code comments for more details on each function and method.

## Contributions
Contributions to this repository are welcome. Please feel free to open a pull request or issue.

## Citation
If you use this code in your research, please cite the corresponding paper:
C. Bongiorno and M. Berritta, Optimal Covariance Cleaning for Heavy-Tailed Distributions: Insights from Information Theory, 2023
